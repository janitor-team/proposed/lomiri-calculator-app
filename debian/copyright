Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lomiri-calculator-app
Source: https://gitlab.com/ubports/development/apps/lomiri-calculator-app

Files: .gitignore
 .gitlab-ci.yml
 AUTHORS
 CMakeLists.txt
 CODE_OF_CONDUCT.md
 README.md
 README-Autopilot.md
 README-Unittest.md
 app/CMakeLists.txt
 app/engine/CMakeLists.txt
 app/engine/README.md
 app/lomiri-calculator-app-splash.svg
 app/lomiri-calculator-app.desktop.in.in
 app/lomiri-calculator-app.in
 app/lomiri-calculator-app.png
 app/lomiri-calculator-app.svg
 app/ui/CMakeLists.txt
 app/upstreamcomponents/CMakeLists.txt
 app/upstreamcomponents/EmptyState.qml
 app/upstreamcomponents/ubuntu_component_store.json
 clickable.yaml
 lomiri-calculator-app.apparmor
 manifest.json.in
 package-lock.json
 package.json
 po/CMakeLists.txt
 po/LINGUAS
 po/POTFILES.in
 tests/CMakeLists.txt
 tests/autopilot/CMakeLists.txt
 tests/autopilot/lomiri_calculator_app/tests/test_main.py
 tests/autopilot/run
 tests/unit/tst_hellocomponent.qml
 screenshot.png
Copyright: 2013-15, Canonical Ltd.
  2017-2019, UBports
License: GPL-3
Comment:
 No license headers in listed files.
 .
 Assuming license and copyright holder from other code files.

Files: app/engine/CalculationHistory.qml
 app/engine/MathJs.qml
 app/engine/formula.js
 app/lomiri-calculator-app.qml
 app/ui/ActionButton.qml
 app/ui/CalcKeyboard.qml
 app/ui/KeyboardButton.qml
 app/ui/KeyboardPage.qml
 app/ui/LandscapeKeyboard.qml
 app/ui/PortraitKeyboard.qml
 app/ui/Screen.qml
 app/ui/ScrollableView.qml
 tests/autopilot/lomiri_calculator_app/tests/__init__.py
Copyright: 2013, 2015, Canonical Ltd.
  2013-2015, Canonical Ltd.
  2014, Canonical Ltd.
  2014-2015, Canonical Ltd.
  2015, Canonical Ltd.
  2017-2019, UBports
License: GPL-3

Files: po/*.po
 po/lomiri-calculator-app.pot
Copyright: 2015, Rosetta Contributors and Canonical Ltd.
  2016, Rosetta Contributors and Canonical Ltd.
  YEAR, Canonical Ltd.
License: GPL-3
Comment:
 Assuming license from other code files.

Files: app/engine/math.js
Copyright: 2013-2020, Jos de Jong <wjosdejong@gmail.com>
License: Apache-2.0

Files: app/upstreamcomponents/MultipleSelectionVisualModel.qml
Copyright: 2012-2015, Canonical Ltd.
License: GPL-3

Files: app/ui/BottomEdgePage.qml
Copyright: 2019, UBports
License: GPL-3

Files: app/ui/TextInputPopover.qml
Copyright: 2015, Canonical Ltd.
License: LGPL-3

Files: tests/autopilot/lomiri_calculator_app/__init__.py
Copyright: 2013, 2016, Canonical Ltd.
  2017-2019, UBports
License: LGPL-3

Files: debian/*
Copyright: 2013-2015, Canonical Ltd.
  2017-2019, UBports
  2022, Guido Berhörster <guido+ubports@berhoerster.name>
  2022, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: LGPL-3

License: GPL-3
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-3
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache 2.0 License
 can be found in /usr/share/common-licenses/Apache-2.0 file.
